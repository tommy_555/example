<?php
/**
 * The DateTimeBehavior converts DB format date in user format.
 *
 * To add an DateTimeBehavior to the model, add it to the behaviors:
 *
 * ```php
 * public function behaviors()
 * {
 *      return [
 *          // ...
 *          [
 *              'class' => DateTimeBehavior::class,
 *              'attributes' => ['create_date', 'update_date'],
 *              //'dbFormat' => 'Y-m-d H:i:s',
 *              //'outputFormat' => 'd.m.Y H:i:s',
 *          ],
 *          // ...
 *      ];
 * }
 * ```
 * 
 */
class DateTimeBehavior extends Behavior
{
    /**
     * @var string the attribute specifies time format for the database.
     */
    public $dbFormat = 'Y-m-d H:i:s';
    /**
     * @var string the attribute specifies time format for the output.
     */
    public $outputFormat = 'd.m.Y H:i:s';
    //...
}